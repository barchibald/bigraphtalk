# Simple socket based protocol
import socket
import subprocess

HOST='0.0.0.0'
PORT=9720

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def recv_file(s):
    BUFFER_SIZE=1024
    d=b''
    while True:
            rec = s.recv(BUFFER_SIZE)
            if b"DONE" in rec:
                rec = rec.replace(b"DONE",b"")
                d += rec
                break
            d += rec
    return d

try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
    sys.exit()

s.listen(1)

while True:
    conn, addr = s.accept()
    json = recv_file(conn)

    with open('tmp','wb') as f:
        f.write(json)

    o=subprocess.check_output("bash gen_model.sh tmp", shell=True)

    conn.sendall(o)

s.close()
