#!/usr/bin/env python

# Simple script to fix up the model such that it can be run with the runtime monitor (for testing)
# Later the export will be made smart enough to not need this

import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument("big", help="BigraphER model file")
parser.add_argument("ml", help="ML model file")
args = parser.parse_args()

# Get a list of everything that should be atomic
atoms = []
ctrls = []
with open(args.big) as f:
    for line in f:
        if re.search("atomic", line):
            m = re.search("ctrl (\w+)", line)
            atoms.append("ctrl_" + m.group(1))
            ctrls.append("ctrl_" + m.group(1))
        else:
            if re.search("ctrl",line):
                m = re.search("ctrl (\w+)", line)
                ctrls.append("ctrl_" + m.group(1))

with open(args.ml) as f:
    for line in f:
        l = line

        # FIXME: Fix params (relies on nodes having a single arg)
        m = re.search("(\[(\w+)\])", l)
        if m:
            l = l.replace(m.group(1),"[Ctrl.S " + m.group(2) + "]")


        # FIXME: Fix reaction params (relies on nodes having a single arg)
        for c in ctrls:
            m = re.search(c + " (-*\d+\.*\d*)", l)
            if m:
                l = l.replace(m.group(1),"\"" + m.group(1) + "\"")

        # Fix atomicity
        for a in atoms:
            if re.search("Big\.ion.*" + a + "[\) ]", l):
                l = l.replace("ion","atom")

        print(l,end='')
