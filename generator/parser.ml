type port = {
    num : int [@key "param_i"];
    type_ : string [@key "param_type"];
    min : float option [@default None];
    max : float option [@default None];
} [@@deriving yojson]

(* Devices *)
type device_feature = {
    df_name : string [@key "df_name"];
    type_ : string [@key "df_type"];
    params : port list [@key "df_parameter"]
} [@@deriving yojson]

type device = {
    dev_id : int [@key "Device_Id"] ;
    dev_model : string [@key "Device_Model"] ;
    dfs : device_feature list [@key "DF_List"]
} [@@deriving yojson]

(* Joins *)
type endpoint = {
    dev_id : int [@key "Device_Id"];
    dev_model : string [@key "Device_Model"];
    dev_df : string [@key "Device_Feature"];
} [@@deriving yojson]

type join_dev = {
    output : endpoint list [@key "Output"];
    input : endpoint list [@key "Input"];
} [@@deriving yojson]

type jfn_type = {
    num : int [@key "param_i"];
    type_ : string [@key "type"] [@default "Any"];
    min : float option [@default None];
    max : float option [@default None];
} [@@deriving yojson]

type join_fn = {
    fn_id : int option [@key "Fn_Id"] [@default Some 1];
    fn_input : jfn_type option list [@key "Fn_Input" ];
    fn_output : jfn_type option list [@key "Fn_Output" ];
} [@@deriving yojson]

type join = {
    jid : int [@key "Join_Id"];
    fn : join_fn [@key "Join_Fn"];
    cons : join_dev [@key "Devices"];
} [@@deriving yojson]

type fdev = {
    mdl : string [@key "Device_Model"];
    loc_id : int [@key "Device_Id"]; (* Local Id *)
    df : string [@key "Device_Feature"];
} [@@deriving yojson]

type fconf = {
    name : string [@key "FC_Name"];
    devs : fdev list [@key "Devices"];
} [@@deriving yojson]

type iottalk_model = {
    devices : device list [@key "Device_List"];
    forbidden_conf : fconf list [@key "Forbidden_Configuration_List"];
    joins : join list [@key "Join_List"];
} [@@deriving yojson]

exception CouldNotParse of string

let parse_model ch =
  let yojson = Yojson.Safe.from_channel ch in
  let res = iottalk_model_of_yojson yojson in
    match res with
        | Ok d -> d
        | Error e -> print_endline ("Could not parse IoTtalk JSON. Error: " ^ e);
                     raise (CouldNotParse e)

let print_model_info mdl =
  print_endline @@ "Devices: " ^ (string_of_int (List.length mdl.devices));
  print_endline @@ "Joins: " ^ (string_of_int (List.length mdl.joins));
