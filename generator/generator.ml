(* Helpers *)
let default (def : 'a) (opt : 'a option) : 'a =
  match opt with
    | Some v -> v
    | None -> def

let rec intersperse_1 list el =
  match list with
  | [] -> list
  | x :: [] -> el :: x :: []
  | x :: y :: tl -> el :: x :: intersperse_1 (y::tl) el

let rec intersperse list el =
  match list with
  | [] | [ _ ] -> list
  | x :: y :: tl -> x :: el :: intersperse (y::tl) el

let flatten_merge ?before_first:(b=false) ls =
    let fold_str (list : string list) = List.fold_left (^) "" list in
  if b then fold_str (intersperse_1 ls "|") else fold_str (intersperse ls "|")

let rec zip xs ys =
  match xs,ys with
 | [],_ -> []
 | _, [] -> []
 | (x :: xs), (y :: ys) -> (x,y) :: (zip xs ys)

(* Link name generators *)
let new_link (r : int ref) (s : string) =
  let v = s ^ string_of_int !r
  in r := !r + 1; v

let jlnk_cnt = ref 0
let dflnk_cnt = ref 0
let ilnk_cnt = ref 0
let olnk_cnt = ref 0
let indlnk_cnt = ref 0
let portlnk_cnt = ref 0

let new_portlink () = new_link portlnk_cnt "prt"
let new_jlink () = new_link jlnk_cnt "j"
let new_dflink () = new_link dflnk_cnt "df"
let new_ilink () = new_link ilnk_cnt "i"
let new_olink () = new_link olnk_cnt "o"
let new_indlink () = new_link indlnk_cnt "ind"

(* Model *)
exception BadModel of string
exception BadComp of string

type port_type =
  | Num of float * float
  | Bool
  | String
  | JSON
  | Any

type port = {
    id : int option;
    mutable links : string list;
    type_ : port_type;
    mutable connected : bool;
    (* For debug info *)
    dev_name : string option;
    df_name : string option;
}

let port_from_json (dev_id : int) (df_name : string ) (p : Parser.port) =
  let t = match String.lowercase_ascii p.type_ with
        | "int" | "float" -> Num (default 0.0 p.min, default 0.0 p.max)
        | "bool" -> Bool
        | "json" -> JSON
        | "string" | "str" -> String
        | "any" -> Any
        | _ -> raise (BadModel ("Unknown port type: " ^ p.type_))
  in
  { id = Some p.num;
    dev_name = Some (string_of_int dev_id);
    df_name = Some df_name;
    links = [];
    type_ = t;
    connected = false
  }

let port_to_str_tc (p: port) =
  let type_to_str (pt : port_type) =
    match pt with
   | Num (min, max) ->
       "Num | Min(" ^ string_of_float min ^ ") | Max(" ^ string_of_float max ^ ")"
   | Bool -> "Bool"
   | JSON -> "JSON"
   | String -> "String"
   | Any -> "Any"
  in
  "Port.("
  ^ (match p.id with
    | Some i -> "Port_Id( " ^ string_of_int i ^ ") | "
    | None -> "")
  ^ type_to_str p.type_
  ^ (if List.length p.links > 0 then
      " | Links.("
        ^ flatten_merge (List.map (fun pl -> "PLink{" ^ pl ^ "}") p.links)
        ^ ")"
    else "")
  ^ (match p.dev_name with
   | Some n -> " | Device_Id(" ^ n ^ ")"
   | None -> "")
  ^ (match p.df_name with
   | Some n -> " | DF_Id(\"" ^ n ^ "\")"
   | None -> "")
  ^ ")"

type device_feature = {
    id : string;
    dev_id : string;
    ports : port list;
}

type idf = {
    df : device_feature;
    mutable out_link : string;
    mutable df_link : string;
}

type ind = {
  name : string;
  fc_name : string;
}

let ind_cmp i j = String.compare i.name j.name

let ind_to_str (i : ind) =
  "Ind{" ^ i.name ^ "}.FC_NAME(\"" ^ i.fc_name ^ "\")"

type odf = {
    df : device_feature;
    mutable in_link : string;
    mutable df_link : string;
    mutable ind : ind list;
}

type df = | Input of idf | Output of odf

let idf_cmp (i : idf) (j : idf) =
  String.compare i.df_link j.df_link

let odf_cmp (i : odf) (j : odf) =
  String.compare i.df_link j.df_link

let df_cmp i j =
  match i,j with
 | Input i1, Input i2 -> idf_cmp i1 i2
 | Output o1, Output o2 -> odf_cmp o1 o2
 | _, _ -> raise (BadComp "Cannot compare IDF and ODF")

let dfs_from_json (dev_id : int) (df : Parser.device_feature) =
  let df' = { id = df.df_name;
              dev_id = string_of_int dev_id;
              ports = List.map (port_from_json dev_id df.df_name) df.params } in
  match df.type_ with
 | "input"  ->  Input {df = df';
                       out_link = new_olink ();
                       df_link = new_dflink ()}
 | "output" ->  Output {df = df'; in_link = new_ilink ();
                        df_link = new_dflink ();
                        ind = []; }
 | _        -> raise (BadModel "Invalid device feature type")

let dfs_to_str_cp (df : df) =
  match df with
 | Input  idf -> "IDF{" ^ idf.out_link ^ "," ^ idf.df_link
                 ^ "}.(DF_Id(\"" ^ idf.df.id ^ "\")"
                 ^ "| Device_Id(" ^ idf.df.dev_id ^ ")"
                 ^ ")"
 | Output odf -> "ODF{" ^ odf.in_link ^ "," ^ odf.df_link
                 ^ "}.(DF_Id(\"" ^ odf.df.id ^ "\") "
                 ^ "| Device_Id(" ^ odf.df.dev_id ^ ") "
                 ^ flatten_merge ~before_first:true (List.map ind_to_str odf.ind)
                 ^ ")"

let dfs_to_str_tc (df : df) =
  let
    output_df lnk prts = "DF{" ^ lnk ^ "}.("
                           ^ flatten_merge (List.map port_to_str_tc prts) ^ ")\\n"
  in
  match df with
   | Input  i -> output_df i.df_link i.df.ports
   | Output o -> output_df o.df_link o.df.ports

type device = {
    id : string;
    mdl : string;
    dfs : df list
}

let dev_to_str_cp (dev : device) =
  "Device.(Device_Id(" ^ dev.id ^ ")" ^
    flatten_merge ~before_first:true (List.map dfs_to_str_cp dev.dfs)
      ^ ")\\n"

let dev_to_str_tc (dev : device) =
    flatten_merge (List.map dfs_to_str_tc dev.dfs)

let dev_from_json (d : Parser.device) =
  { id = string_of_int d.dev_id; mdl = d.dev_model; dfs = List.map (dfs_from_json d.dev_id) d.dfs }

(* Joins/Join Fns *)
type join = {
    id : string;
    jfnlink : string;
    mutable inlinks : string list;
    mutable outlinks : string list;
    in_ports : port list;
    out_ports : port list;
}

let join_to_str_cp (j : join) =
  let joinpt_to_str (s : string) = "Join_pt{" ^ s ^ "}" in
  "Join{" ^ j.jfnlink ^ "}.(Join_Id(" ^ j.id ^ ")"
  ^ flatten_merge ~before_first:true (List.map joinpt_to_str j.inlinks)
  ^ flatten_merge ~before_first:true (List.map joinpt_to_str j.outlinks)
  ^ ")\\n"

let join_to_str_tc (j : join) =
  let output_ins ps = "Join_Fn_Ins.(" ^ flatten_merge (List.map port_to_str_tc ps) ^ " | Join_Id(" ^ j.id ^ ")" ^ ")" in
  let output_outs ps = "Join_Fn_Outs.(" ^ flatten_merge (List.map port_to_str_tc ps) ^ " | Join_Id(" ^ j.id ^ ")" ^ ")" in
  "Join_Fn{" ^ j.jfnlink ^ "}.("
  ^ output_ins j.in_ports
  ^ " | "
  ^ output_outs j.out_ports
  ^ ")\\n"

(* For searching in the devices *)
let get_df (df : df) =
  match df with
 | Input i ->  i.df
 | Output o -> o.df

let get_id (df : df) = let d = get_df df in d.id
let get_ports (df : df) = let d = get_df df in d.ports

let get_ioname (df : df) =
  match df with
 | Input i ->  i.out_link
 | Output o -> o.in_link


let df_has_id (df : df) (df_id : string) =
  get_id df = df_id

let find_df (devices : device list) (dev_id : string) (df_name : string) =
  let ds = List.filter (fun (d : device) -> d.id = dev_id) devices in
  let dfs = List.concat (List.map (fun d -> List.filter (fun df -> df_has_id df df_name) d.dfs) ds) in
  List.hd @@ dfs

let get_links (devs : device list) (e : Parser.endpoint) =
  let d = find_df devs (string_of_int e.dev_id) e.dev_df
  in get_ioname d

let jfn_from_json (j : Parser.jfn_type option) : port =
  let jfn_type_to_port (jfn_t : Parser.jfn_type)  =
    let t = match String.lowercase_ascii jfn_t.type_ with
        | "int" | "float" -> Num (default 0.0 jfn_t.min, default 0.0 jfn_t.max)
        | "bool" -> Bool
        | "json" -> JSON
        | "string" | "str" -> String
        | "any" -> Any
        | _ -> raise (BadModel ("Unknown port type: " ^ jfn_t.type_))
    in
    match t with
    | Any -> { id = Some jfn_t.num; dev_name = None; df_name = None; links = [new_portlink ()]; type_ = Any; connected = false;}
    | _   -> { id = Some jfn_t.num; dev_name = None; df_name = None; links = [new_portlink ()]; type_ = t; connected = false;}
  in
  match j with
    | Some x -> jfn_type_to_port x
    | None -> { id = None; dev_name = None; df_name = None; links = [new_portlink ()]; type_ = Any; connected = false;}

let connect_ports (j : Parser.join) (devs : device list) (join : join) =
  let get_dfs (e : Parser.endpoint) = get_df (find_df devs (string_of_int e.dev_id) e.dev_df) in
  let rec update_links prts df =
    match prts, df with
   | [],_ -> prts
   | _,[] -> prts
   | (x::xs), (y::ys) ->
      let nm = List.hd x.links in
      y.links <- List.sort_uniq compare (nm :: y.links);
      x.connected <- true;
      update_links xs ys
  in
  let idfs = (List.map get_dfs j.cons.input) in
  let odfs = (List.map get_dfs j.cons.output) in
  let _prts = List.fold_left update_links join.in_ports (List.map (fun df -> df.ports) idfs) in
  let _prts = List.fold_left update_links join.out_ports (List.map (fun df -> df.ports) odfs) in
  ()

let update_unconnected join =
  let update prt = if prt.connected then () else prt.links <- ["uncon"] in
  List.iter update join.in_ports;
  List.iter update join.out_ports

let join_from_json (dev : device list) (j : Parser.join) =
  let join = {
    id = string_of_int j.jid;
    jfnlink = new_jlink ();
    inlinks = List.map (get_links dev) j.cons.input;
    outlinks = List.map (get_links dev) j.cons.output;
    in_ports = List.map jfn_from_json j.fn.fn_input;
    out_ports = List.map jfn_from_json j.fn.fn_output;
  } in
  let () = connect_ports j dev join in
  let () = update_unconnected join in
  join

type model = {
    devices : device list;
    joins : join list;
}

let output_ind_closure (n : int) =
  List.init n (fun x -> x)
  |> List.map (fun i -> "/ind" ^ string_of_int i)
  |> String.concat " "

let model_to_str (mdl : model) =
  let output_cp =
      output_ind_closure !indlnk_cnt ^ " "
    ^ "ConnectionPerspective.("
    ^ flatten_merge (List.map dev_to_str_cp mdl.devices)
    ^ flatten_merge ~before_first:true (List.map join_to_str_cp mdl.joins)
    ^ ")"
  in
  let output_tc =
    "TypeCheckingPerspective.("
    ^ flatten_merge (List.map dev_to_str_tc mdl.devices)
    ^ flatten_merge ~before_first:true (List.map join_to_str_tc mdl.joins)
    ^ "| MissingArgs{uncon}"
    ^ ")"
  in
  output_cp ^ "\\n || " ^ output_tc

let has_df (d : device) (df_name : string) =
  let ds = List.filter (fun df -> get_id df = df_name) d.dfs in
  List.length ds > 0

(* Cartesian Product of n-lists *)
let rec cproduct l =
  let rec prod ~acc l1 l2 =
    match l1, l2 with
   | [], _ -> acc
   | _, [] -> acc
   | x::xs, y::ys ->
      let acc = (x::y)::acc in
      let acc = (prod ~acc xs l2) in
      prod ~acc [x] ys
  in match l with
    | [] -> []
    | [l1] -> List.map (fun x -> [x]) l1
    | l1::tl ->
       let tail_product = cproduct tl in
       prod ~acc:[] l1 tail_product

let filter_sets (ls : df list list list) : (df list list) =
  (* [[1],[1,2] -> Discard, [[1],[2]] -> [1,2]*)
  let filter_set all_sets xs =
    let l = List.concat xs in
    let usz = List.sort_uniq df_cmp l |> List.length in
    if List.length l == usz then
     l :: all_sets
    else
     all_sets
  in
  List.fold_left filter_set [] ls

let update_forbidden_conf (ds : device list) (conf : Parser.fconf) : unit =
  let fconfDevsToMap (devs : Parser.fdev list) =
    let tbl = Hashtbl.create 50 in
    let () = List.iter (fun (d : Parser.fdev) ->
        let lst = Hashtbl.find_opt tbl d.loc_id in
        match lst with
       | Some (dm, dfs) -> Hashtbl.replace tbl d.loc_id (dm, d.df :: dfs)
       | None -> Hashtbl.replace tbl d.loc_id (d.mdl,[d.df])
      ) devs
    in tbl
  in
  let find_dfs dtbl : df list list list =
    let has_all_dfs dfs dev =
      List.for_all (fun df ->
          List.exists (fun (ddf : df) -> let df' = get_df ddf in df'.id = df) dev.dfs
        ) dfs
    in
    let get_dfs dfs dev =
      List.map (fun df ->
          List.find (fun (ddf : df) -> let df' = get_df ddf in df'.id = df) dev.dfs
      ) dfs
    in
    let get_all_dfs dm dfs : (df list list) =
      List.filter (fun (d : device) -> d.mdl = dm) ds
        |> List.filter (has_all_dfs dfs)
        |> List.map (get_dfs dfs)
    in
    Hashtbl.fold (fun _devid (mdl, features) acc ->
        (get_all_dfs mdl features) :: acc
      ) dtbl []
  in
  let update_dfs dfs =
    let update_df i df =
        match df with
          | Input _ -> raise (BadModel "Trying to make an input independent")
          | Output o ->
             let ind = { name = i; fc_name = conf.name } in
                       o.ind <- List.sort_uniq ind_cmp (ind :: o.ind)
    in
    let ind = new_indlink () in
    List.iter (update_df ind) dfs
  in
  let tbl = fconfDevsToMap conf.devs in
  let devs = find_dfs tbl in
  let cmbs = cproduct devs in
  let cmbs' = filter_sets cmbs in
  List.iter update_dfs cmbs'

let model_from_json (m : Parser.iottalk_model) =
  let devices = List.map dev_from_json m.devices in
  let () = List.iter (update_forbidden_conf devices) m.forbidden_conf in
  let joins = List.map (join_from_json devices) m.joins in
  { devices = devices;
    joins   = joins
  }

let () =
  let jsonmdl = Parser.parse_model stdin in
  let mdl' = model_from_json jsonmdl in
  print_endline @@ model_to_str mdl'
