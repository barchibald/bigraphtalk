let
    pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/20.09.tar.gz") {};
    bigrapher = with pkgs;
      stdenv.mkDerivation rec {
        name = "BigraphER";
        version = "1.9.3";

        src = fetchGit {
          url = "https://bitbucket.org/uog-bigraph/bigraph-tools";
          ref = "ml_export";
          rev = "ac735d7c7e348e26dde54c1f06870252b23703d9";
        };

        buildInputs = with ocamlPackages; [ pkgs.minisat zlib jsonm cmdliner ];
        nativeBuildInputs = with ocamlPackages; [ dune_2 ocaml findlib
                                                  dune-configurator menhir ];

        buildPhase = ''
            dune build @install --profile=release
          '';

        installPhase = ''
          # dune install bigraph --prefix $out --libdir $OCAMLFIND_DESTDIR bigraph
          # dune install minicard --prefix $out --libdir $OCAMLFIND_DESTDIR minicard
          # dune install minisat --prefix $out --libdir $OCAMLFIND_DESTDIR minisat
          # dune install bigrapher --prefix $out
          dune install --prefix $out --libdir $OCAMLFIND_DESTDIR
        '';
      };
in
pkgs.mkShell {
  buildInputs =
    [
      bigrapher
      pkgs.dune_2
      pkgs.ocaml
      pkgs.opam
      pkgs.ocamlPackages.findlib
      pkgs.ocamlPackages.ocamlbuild
      pkgs.ocamlPackages.yojson
      pkgs.ocamlPackages.ppx_deriving_yojson
      pkgs.zlib
      pkgs.stdenv
      pkgs.gnumake
      pkgs.python3Full
      pkgs.minisat
    ];
}
