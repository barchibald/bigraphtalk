#!/usr/bin/env bash

fname=$1
if [[ -z $fname ]]; then
    echo "Usage: gen_mode.sh <user-app-fname>"
    exit -1
fi

big=$(cat $fname | generator/_build/default/generator.exe)
sed -e "s:<<USER_APP>>:${big}:g" meta-model.big >mdl.big

bigrapher validate -m tmp.ml mdl.big >/dev/null
python fix_export.py mdl.big tmp.ml >tmp2.ml
head -n -3 tmp2.ml >matcher/model.ml

pushd matcher &>/dev/null
make &>/dev/null
popd &>/dev/null

matcher/_build/default/matcher.exe

rm -f lbls mdl.big tmp tmp.ml tmp2.ml matcher/model >/dev/null
