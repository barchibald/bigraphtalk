# BigraphTalk

BigraphTalk is a verification layer for IoT-Talk models. It is based on Bigraphs
[1], and makes use of the
[BigraphER](http://www.dcs.gla.ac.uk/~michele/bigrapher.html) tool for computing
with bigraphs.

# Installing and Running

The easiest way to run/test BigraphTalk is through the provided [Docker](https://docs.docker.com/):

``` sh
docker build --tag=bigtalk .
docker run bigtalk
```

Alternatively you can use [Nix](https://nixos.org/) and build from source:

``` sh
nix-shell
(cd generator && make)
python3 run.py # should be run inside the nix-shell
```

BigraphTalk listens for a JSON encoded IoTtalk model on port 9720. Once it is
running you can test it using:

``` sh
cat test-files/fig3.txt | nc <bigraph-talk-ip> 9720
```

You should receive a JSON encoded feedback message (or empty JSON if there are
no errors in the model). The `bigraph-talk-ip` is either localhost or may be
found by running docker inspect on the BigraphTalk container.

# References

- [1] R. Milner, The space and motion of communicating agents. Cambridge University Press, 2009
