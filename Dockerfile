FROM nixos/nix

WORKDIR /BigraphTalk
COPY . /BigraphTalk

RUN nix-channel --add https://nixos.org/channels/nixos-22.05 nixpkgs
RUN nix-channel --update

RUN nix-env -iA \
    nixpkgs.coreutils \
    nixpkgs.gnutar \
    nixpkgs.gzip \
    nixpkgs.git \
    nixpkgs.bash

RUN nix-shell shell.nix --command 'pushd generator; make; popd'

EXPOSE 9720

CMD ["bash", "run.sh"]
