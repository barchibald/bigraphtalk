open Bigraph

exception BAD_OPTIONAL
let force_get = function
  | Some x -> x
  | None -> raise BAD_OPTIONAL

(* Assumes ctrls are one step from the root which is fine for BigraphTalk *)
let region_ctrls (r : int) (p : Place.t) (n : Nodes.t) =
  Sparse.fold (fun i j acc -> if i == r then j :: acc else acc ) p.rn []
    |> List.map (fun i -> force_get @@ Nodes.get_ctrl i n)

(* Replicated from Bigrapher *)
let string_of_param = function
  | Ctrl.I i -> string_of_int i
  | Ctrl.F f -> string_of_float f
  | Ctrl.S s -> s

let get_param = function
  | Ctrl.C (c, [p], _) -> string_of_param p
  | _ -> ""

exception MISSING_INFO of string

let get_param_ctrl_nm nm ctrls =
  let ctrl = List.filter (fun c -> String.compare (Ctrl.name c) nm == 0) ctrls in
  let c = match ctrl with
    | [] -> raise (MISSING_INFO ("Can't find control of name: " ^ nm ^ " Got: " ^ (String.concat "; " (List.map Ctrl.name ctrls))))
    | xs -> List.hd xs
  in get_param c

let find_ctrl_params c (mtch : Big.t) : string list =
    let is = Nodes.find_all c mtch.n in
    IntSet.fold (fun i acc -> match Nodes.get_ctrl i mtch.n with
                              | Some c -> acc @ [get_param c]
                              | None -> acc) is []

let find_join_ids (mtch : Big.t) : string list =
 find_ctrl_params (Model.ctrl_Join_Id "") mtch |> List.sort_uniq compare

let rec zip xs ys =
  match xs,ys with
 | [],_ -> []
 | _, [] -> []
 | (x :: xs), (y :: ys) -> (x,y) :: (zip xs ys)
