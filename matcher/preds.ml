let preds =
  [
    (Model.forbidden_conf_1_2, Handlers.forbidden_conf_1_handler [1;3] [2;4] [0]) ;
    (Model.forbidden_conf_1_3, Handlers.forbidden_conf_1_handler [1;3;5] [2;4;6] [0]) ;
    (Model.forbidden_conf_2_2, Handlers.forbidden_conf_2_handler [3;5] [4;6] [1;2]) ;
    (Model.forbidden_conf_2_3_1, Handlers.forbidden_conf_2_handler [4;6;8] [5;7;9] [1;2;3]) ;
    (Model.forbidden_conf_2_3_2, Handlers.forbidden_conf_2_handler [3;5;7] [4;6;8] [1;2]) ;

    (Model.bad_type_in_bool_num, Handlers.bad_type_handler "Bool" "Num") ;
    (Model.bad_type_in_num_bool, Handlers.bad_type_handler "Num" "Bool") ;
    (Model.bad_type_in_bool_str, Handlers.bad_type_handler "Bool" "String") ;
    (Model.bad_type_in_str_bool, Handlers.bad_type_handler "String" "Bool") ;
    (Model.bad_type_in_bool_json, Handlers.bad_type_handler "Bool" "JSON") ;
    (Model.bad_type_in_json_bool, Handlers.bad_type_handler "JSON" "Bool") ;
    (Model.bad_type_in_num_str, Handlers.bad_type_handler "Num" "String") ;
    (Model.bad_type_in_str_num, Handlers.bad_type_handler "String" "Num") ;
    (Model.bad_type_in_num_json, Handlers.bad_type_handler "Num" "JSON") ;
    (Model.bad_type_in_json_num, Handlers.bad_type_handler "JSON" "NUM") ;
    (Model.bad_type_in_str_json, Handlers.bad_type_handler "String" "JSON") ;
    (Model.bad_type_in_json_str, Handlers.bad_type_handler "JSON" "String") ;

    (* Note direction switch on types to ensure correct debug messages*)
    (Model.bad_type_out_bool_num, Handlers.bad_type_handler "Num" "Bool" ) ;
    (Model.bad_type_out_num_bool, Handlers.bad_type_handler "Bool" "Num" ) ;
    (Model.bad_type_out_bool_str, Handlers.bad_type_handler "String" "Bool" ) ;
    (Model.bad_type_out_str_bool, Handlers.bad_type_handler "Bool" "String" ) ;
    (Model.bad_type_out_bool_json, Handlers.bad_type_handler "JSON" "Bool" ) ;
    (Model.bad_type_out_json_bool, Handlers.bad_type_handler "Bool" "JSON" ) ;
    (Model.bad_type_out_num_str, Handlers.bad_type_handler "String" "Num" ) ;
    (Model.bad_type_out_str_num, Handlers.bad_type_handler "Num" "String" ) ;
    (Model.bad_type_out_num_json, Handlers.bad_type_handler "JSON" "Num" ) ;
    (Model.bad_type_out_json_num, Handlers.bad_type_handler "NUM" "JSON" ) ;
    (Model.bad_type_out_str_json, Handlers.bad_type_handler "JSON" "String" ) ;
    (Model.bad_type_out_json_str, Handlers.bad_type_handler "String" "JSON" ) ;

    (Model.missing_args_in, Handlers.missing_args_handler) ;
    (Model.missing_args_out, Handlers.missing_args_handler) ;

    (Model.param_mismatch_0_1, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_1_0, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_0_2, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_2_0, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_0_3, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_3_0, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_1_2, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_2_1, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_1_3, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_3_1, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_2_3, Handlers.param_mismatch_handler) ;
    (Model.param_mismatch_3_2, Handlers.param_mismatch_handler)
  ]
