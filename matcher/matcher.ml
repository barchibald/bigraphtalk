open Bigraph
module S = Solver.Make_SAT (Solver.MS)

let get_errors b (pred, handler) =
  let occs = S.occurrences ~target:b ~pattern:pred in
  let mtchs =
    List.map
    (fun (o : Solver.occ) -> Big.decomp ~target:b ~pattern:pred ~i_n:o.nodes ~i_e:o.edges o.hyper_edges;)
    occs
  in
  List.map (fun (_c,d,_id) -> handler d) mtchs

let () =
  let initial_state = Model.init_tmp_big in
  let errors = List.map (get_errors initial_state) Preds.preds in
  print_endline (Yojson.Safe.to_string (Handlers.errors_to_yojson (errors |> List.concat)));
  print_endline "DONE"
