open Bigraph
open Util

type df = {
    device  : string [@key "device_id"];
    df_name : string [@key "df_name"];
} [@@deriving yojson]

let df_to_str df = df.device ^ "," ^ df.df_name

type conn_info = {
    join_ids : string list [@key "join_ids"];
    device_ids : df list [@key "devices"];
} [@@deriving yojson]

type forbidden_conf = {
    fc_name : string;
    conn_info : conn_info
} [@@deriving yojson]

let fp_2_to_str fp = "Joins:" ^ String.concat " " fp.join_ids ^
                     "Devices; " ^ String.concat " " (List.map df_to_str fp.device_ids)

type bad_type = {
    join_id : string [@key "join_id"];
    device : df [@key "device"];
} [@@deriving yojson]

type missing_args = {
    join_id : string [@key "join_id"];
} [@@deriving yojson]

type param_mismatch = {
    join_id : string [@key "join_id"];
    device : df [@key "device"];
} [@@deriving yojson]

type error =
  | Forbidden_conf_1 of forbidden_conf
  | Forbidden_conf_2 of forbidden_conf
  | Type_error of bad_type
  | Missing_args of missing_args
  | Parameter_mismatch of param_mismatch
  [@@deriving yojson]

type error_type = {
    error_type : string;
    data : error
} [@@deriving yojson]

type errors = error_type list [@@deriving yojson]

let  get_ctrls_from_regions (c : string) (mtch : Big.t) (rs : int list) =
  List.map (fun r -> get_param_ctrl_nm c (region_ctrls r mtch.p mtch.n)) rs

let forbidden_conf_handler rinds rdevs rjoins (mtch : Big.t) =
  let devices = get_ctrls_from_regions "Device_Id" mtch rdevs in
  let dfs = get_ctrls_from_regions "DF_Id" mtch rdevs in
  let dev_info = zip devices dfs |> List.map (fun (d,df) -> { device = d; df_name = df} ) in
  { fc_name = get_ctrls_from_regions "FC_NAME" mtch rinds
              |> List.sort_uniq compare |> String.concat " ";
    conn_info = {
        join_ids = get_ctrls_from_regions "Join_Id" mtch rjoins;
        device_ids = dev_info
    }
  }

let forbidden_conf_1_handler rinds rdevs rjoins (mtch : Big.t) =
  {
    error_type = "Forbidden Configuration" ;
    data = Forbidden_conf_1 ( forbidden_conf_handler rinds rdevs rjoins mtch)
  }

let forbidden_conf_2_handler rinds rdevs rjoins (mtch : Big.t) =
  {
    error_type = "Possible Forbidden Configuration";
    data = Forbidden_conf_2 ( forbidden_conf_handler rinds rdevs rjoins mtch)
  }

let bad_type_handler (t1 : string) (t2 : string) (mtch : Big.t) =
  let dev_region = 1 in
  let idf1 = region_ctrls dev_region mtch.p mtch.n in
  let device_1 = get_param_ctrl_nm "Device_Id" idf1 in
  let df_1 = get_param_ctrl_nm "DF_Id" idf1 in
  let e = Type_error ({
    join_id = get_ctrls_from_regions "Join_Id" mtch [4] |> List.hd;
    device = { device = device_1; df_name = df_1 }
  })
  in
  {
    error_type = "Type mismatch: " ^ t1 ^ " does not unify with " ^ t2;
    data = e
  }

let missing_args_handler (mtch : Big.t) =
  let uncon_region = 2 in
  let e = Missing_args ({
    join_id = get_ctrls_from_regions "Join_Id" mtch [uncon_region] |> List.hd;
  })
  in
  {
    error_type = "Missing argument";
    data = e
  }

let param_mismatch_handler (mtch : Big.t) =
  let uncon_region = 4 in
  let e = Missing_args ({
    join_id = get_ctrls_from_regions "Join_Id" mtch [uncon_region] |> List.hd;
  })
  in
  {
    error_type = "Parameter mismatch";
    data = e
  }
